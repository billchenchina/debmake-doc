[[ci]]
=== Continuous integration

https://dep-team.pages.debian.net/deps/dep8/[DEP-8] defines the *debian/tests/control* file as the RFC822-style test metadata file for https://en.wikipedia.org/wiki/Continuous_integration[continuous integration] (CI) of the Debian package.

It is used after building the binary packages from the source package containing this *debian/tests/control* file.  When the *autopkgtest* command is run, the generated binary packages are installed and tested in the virtual environment according to this file.

See documents in the */usr/share/doc/autopkgtest/* directory and http://packaging.ubuntu.com/html/auto-pkg-test.html[4. autopkgtest: Automatic testing for packages] of the ``Ubuntu Packaging Guide''.

NOTE: Testing of the binary packages during their building time can be accomodated by `dh_auto_test`.

There are several other CI tools on Debian for you to explore.

* The *debci* package: CI platform on top of the *autopkgtest* package
* The *jenkins* package: generic CI platform

=== Other new requirements

Debian packaging practices are moving target.  Please keep your eyes on https://dep-team.pages.debian.net/[DEP - Debian Enhancement Proposals].

