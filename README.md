# Debmake documentation

This is the source file for debmake-doc package.

 * https://salsa.debian.org/debian/debmake-doc (upstream site)
 * https://www.debian.org/doc/devel-manuals#debmake-doc (build pages)
 * https://www.debian.org/doc/manuals/debmake-doc/index.en.html (English content)

All files included in this source tree are under the MIT license with its text
in the LICENSE file except for the files which explicitly claim differently in
them.

## Git repository

NOTE: use of "devel" branch is deprecated.

All updates should be committed to "main" branch of the Git repository at

  https://salsa.debian.org/debian/debmake-doc

The "main" branch is meant to build the non-native Debian binary package.  The
version number in debian/changelog is non-native version number.  This works
since this is also upstream repo.  For minor updates, we can use -2 revision,
too.  See "man dgit-maint-merge".

Test build:

```
    $ git deborig -f HEAD
    $ sbuild
```

Playing with git purely from command line to track changes isn't easy.  Please
consider using GUI tool such as gitk.

When building revision packages such as -2, -3, ..., obtain and use the
original tar file used for -1 revision instead of using `git deborig -f HEAD`.

## Build system

The build system involves several `Makefile`s for make:

* Makefile -- "package" target does "git deborig -f HEAD; sbuild"
* examples/Makefile -- "all" target to generate the packaging example logs

For `sbuild` configuration, see https://wiki.debian.org/sbuild .

### Normal build

The packaging of debmake-doc is:

* package build by "make package" from the git "main" branch.
* upload package with "dput" (source only for Debian)

### Pre-build

Prior to the execution of "make package", the up-to-date packaging example
logs need to created as pre-build and committed to the source tree in the
testing environment (See details in [examples/README.md](examples/README.md) ).

```
 $ make clean
 $ cd examples
 $ make
 $ make clean
 $ git add -A .
 $ git commit -m "Update example logs"
```

### Preliminary debug build of single-page English html

If you are debugging ASCIIDOC markup issues with newly updated build log
examples and wish to test the ASCIIDOC source quickly without going through
XML, here is an alternative html build method:

```
 $ cd asciidoc
 $ make
```

The result is asciidoc/debameke-doc.html

Please commit English asciidoc source text contents to git repository after
making sure all the above are bug free.

This also build updated mangage which can be copied to the debmake package.

### Debug build (English updates)

Update English original content by hacking ASCIIDOC text files under asciidoc/
directory.  The result of debug build can be previewed with the browser by
generating html files.

```
 $ make LANGPO= doc-test
```

Please note that `LANGPO` is set to empty string in the above to skip translation.

Results are found under basedir/html/ .

If you get error free html file, you may wish to check PDF file.

```
 $ make LANGPO= pdf  # preview PDF
```

Results are found under basedir/ .

### Debug build (non-English updates)


Before working on updating translation, please make sure to refresh files
under `po4a/po/` directory (a.k.a.  PO files) for your language `<lang>` from the
latest asciidoc text contents.

```
 $ git pull
 $ make LANGPO=<lang> po
 ... update po4a/po/<lang>.po
 $ make LANGPO=<lang> po
```

You can translate this source by hacking PO files for
`LANGPO=<lang>` under `po4a/po/` directory using tools such as poedit.

The result of debug build can be previewed with the browser by generating html
files in the way almost the same as "Debug build (English updates)" while
specifying the `<lang>`.

```
 $ make LANGPO=<lang> doc-test
```

If you get error free html file, you may wish to check PDF file too.

```
 $ make LANGPO=<lang> pdf  # preview PDF
```

Results are found under `basedir/` .

If the result of your translation previewed with the browser and PDF viewer
are good, please commit `po4a/po/<lang>.po` only as below.

```
 $ git add po4a/po/<lang>.po
 $ git commit -m "Updated <lang>.po"
 $ git reset --hard HEAD
 $ git clean -d -f -x
```

### Translation in po4a/po directory

This directory contain po files for translators.  This is done using po4a.

If you updated a PO file while others committing the same file before you
commit it, merging of the PO file isn't trivial.  Don't try to merge just by
git.

There is a `bin/msgmsguniq` script to help merging 2 po files.

Let's say to merge `<lang>.po.my` and `<lang>.po.their`.

```
 $ bin/msgmsguniq <lang>.po.my <lang>.po.their > <lang>.po
```

Then move this generated`<lang>.po` to the `po4a/po/<lang>.po` and normalize it with:

```
 $ make LANGPO=<lang> po
```

Now you have merged PO to `po4a/po/<lang>.po`.

You may un-fuzzy manually and commit the clean tested `po4a/po/<lang>.po` to
git repo.

### Files in `asciidoc/`

This directory contains the source text files written in the asciidoc markup.
Their extension is usually .txt .

### Source upload

Make source package and record it to git:

```
    $ git reset --hard HEAD
    $ git clean -d -f -x
    $ dpkg-buildpackage -S --no-sign
    $ pristine-tar ../debmake-doc_1.16.orig.tar.xz main
    $ cd ..
    $ debsign debmake-doc_1.16-1_source.changes
    $ dput debmake-doc_1.16-1_source.changes
```

Use of the `debsign` command is desirable to avoid picking old 1024 bit keys.

<!-- vim:se tw=78 sts=4 ts=4 et ai: -->
